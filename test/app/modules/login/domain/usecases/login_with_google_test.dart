import 'package:dartz/dartz.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:login_with_clean/app/core/connectivity/domain/connectivity_service.dart';
import 'package:login_with_clean/app/core/errors/messages.dart';
import 'package:login_with_clean/app/modules/login/domain/entities/user.dart';
import 'package:login_with_clean/app/modules/login/domain/errors/errors.dart';
import 'package:login_with_clean/app/modules/login/domain/repositories/login_repository.dart';
import 'package:login_with_clean/app/modules/login/domain/usecases/login_with_google.dart';
import 'package:mocktail/mocktail.dart';

class LoginRepositoryMock extends Mock implements LoginRepository {}
class ConnectivityServiceMock extends Mock implements ConnectivityService {}


main() {

  final repository = LoginRepositoryMock();
  final connectivityService = ConnectivityServiceMock();

  final usecase = LoginWithGoogleImpl(repository, connectivityService);

  test("test Failure call usecase", () async {

    when(() => connectivityService.isOnline()).thenAnswer((_) async => false);

    final result = await usecase();

    expect(result.leftMap((failure) => failure is ErrorLogin), Left(true));
    expect(result.fold((failure) => failure.message, id), Messages.OFFILINE_CONNECTION);

  });


  test("test Success call usecase", () async {

    final user = User(uid: "4231", email: "user@email.com", name: "User");

    when(() => connectivityService.isOnline()).thenAnswer((_) async => true);
    when(() => repository.executeLoginGoogle()).thenAnswer((_) async => Right(user));

    final result = await usecase();

    expect(result, Right(user));
    expect(result.fold(id, (user) => user.name), "User");

  });

}